#!/usr/bin/env bash
# set -x
set -e

DOTFILES_FOLDER="${HOME}/dotfiles"
DOTFILES_DEPS_BIN="${DOTFILES_FOLDER}/deps/bin"

mkdir -p ${DOTFILES_DEPS_BIN}

wget raw.githubusercontent.com/qzb/is.sh/latest/is.sh -O is
mv is ${DOTFILES_DEPS_BIN}
chmod +x ${DOTFILES_DEPS_BIN}/is

wget git.io/ansi -O ansi
mv ansi ${DOTFILES_DEPS_BIN}
chmod +x ${DOTFILES_DEPS_BIN}/ansi

source ${DOTFILES_DEPS_BIN}/ansi
source ${DOTFILES_DEPS_BIN}/is

rmpackages=(
    docker
    docker-engine
    docker.io
)

ppakeys=(
)

ppas=(
)

packages=(
    apt-transport-https
    ca-certificates
    software-properties-common
    silversearcher-ag
    vim
    curl
    git
)

bashcurls=(
    "get.docker.com"
)

confs=(
    "zsh/bashrc.bashit"
    "vim/vimrc"
)
confspath=(
    ".bashrc"
    ".vimrc"
)

aftercommands=(
    # "vim/update.sh"
    # "sudo usermod -aG docker ${USER}"
    "git clone --depth=1 https://github.com/Bash-it/bash-it.git ${HOME}/.bash_it && ${HOME}/.bash_it/install.sh"
)

if is not empty "${ppakey[@]}"; then
    ansi --green --newline "Begin install for ppa keys ..."
    for ppakey in "${ppakeys[@]}"; do
        curl -fsSL ${ppakey} | sudo apt-key add -
    done
    ansi --green --newline "All ppa keys installed"
else
    ansi --yellow --newline "No ppas keys to install"
fi

if is not empty "${ppas[@]}"; then
    ansi --green --newline "Begin install for ppas ..."
    for ppa in "${ppas[@]}"; do
        eval ${ppa}
    done
    ansi --green --newline "All ppas installed."
else
    ansi --yellow --newline "No ppas to install."
fi

if is not empty "${rmpackages[@]}"; then
    ansi --green --newline "Begin remove for package ..."
    sudo apt purge -y "${rmpackages[@]}"
    ansi --green --newline "All packages removed."
else
    ansi --newline "All packages removed."
fi

ansi --green --newline "Update repositories ..."
sudo apt update | echo "Some repo failed."

if is not empty "${packages[@]}"; then
    ansi --green --newline "Begin install for package ..."
    sudo apt install -y "${packages[@]}"
    . "${HOME}/.bashrc"
    ansi --green --newline "All packages installed ..."
else
    ansi --newline "No packages to installed."
fi

if is not empty "${bashcurls[@]}"; then
    for bashcurl in "${bashcurls[@]}"; do
        ansi --newline "Begin install of curl script ... "
        sh -c "$(curl -fsSL ${bashcurl})"
        ansi --newline "All packages installed."
    done
else
    ansi --newline "No curl script to install."
fi


if is not empty "${aftercommands[@]}"; then
    ansi --newline "Begin aftercommands ..."
    for command in "${aftercommands[@]}"; do
        sh -c "${command}"
    done
    ansi --newline "aftercommands executed succesfully."
else
    ansi --newline "No aftercommands to execute."
fi


if is not empty "${confs[@]}"; then
i=0
for conf in "${confs[@]}"; do
    ansi --newline "Begin install for confs ..."
    ln -sf "`pwd`/${conf} ${HOME}/${confspath[i]}"
    let i=${i}+1
    ansi --newline "Conf install ended succesfully."
done
else
    ansi --newline "No confs to install."
fi
